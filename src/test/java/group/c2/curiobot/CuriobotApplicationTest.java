package group.c2.curiobot;

import org.junit.jupiter.api.Test;

public class CuriobotApplicationTest {
    @Test
    public void test()
    {
        CuriobotApplication.main(new String[]{
                "--spring.main.web-environment=false",
                "--spring.autoconfigure.exclude=blahblahblah",
                // Override any other environment properties according to your needs
        });
    }
}
