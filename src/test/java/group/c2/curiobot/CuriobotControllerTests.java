package group.c2.curiobot;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.ImageMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import group.c2.curiobot.utils.EventTestUtility;
import group.c2.curiobot.utils.ObjectMapperUtility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(properties = "line.bot.handler.enabled=false", classes = CuriobotApplication.class)
@ExtendWith(SpringExtension.class)
class CuriobotControllerTests {

	static {
		System.setProperty("line.bot.channelSecret", "DUMMYSECRET");
		System.setProperty("line.bot.channelToken", "DUMMYTOKEN");
	}

	@Autowired
	private CuriobotController curiobotController;

	private final EventTestUtility eventTestUtility = new EventTestUtility();
	private final ObjectMapperUtility objectMapperUtility = new ObjectMapperUtility();

	void testBotModelResponse(Object originalModel) {
		Object reconstructedModel = this.objectMapperUtility.serializeThenDeserialize(originalModel);
		assertThat(reconstructedModel).isEqualTo(originalModel);
	}

	@Test
	void testContextLoads() {
		assertNotNull(curiobotController);
	}

	@Test
	void testShowHelpButtonsFlex() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".help");
		Message reply = curiobotController.handleTextEvent(event);
		this.testBotModelResponse(reply);
	}

	@Test
	void testHelpButtonsFlexContents() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".help");
		Message replyFlex = curiobotController.handleTextEvent(event);
		String replyFlexJson = objectMapperUtility.serialize(replyFlex);

		assertThat(replyFlexJson).contains("\"altText\":\"HELP\"");
		assertThat(replyFlexJson).contains("\"url\":\"https://i.ibb.co/BrNz4DL/robot-1.png\"");
		assertThat(replyFlexJson).contains("\"text\":\"FEATURES\"");
		assertThat(replyFlexJson).contains("\"label\":\".wiki\"");
		assertThat(replyFlexJson).contains("\"label\":\".news\"");
		assertThat(replyFlexJson).contains("\"label\":\".image\"");
		assertThat(replyFlexJson).contains("\"label\":\".facts\"");
		assertThat(replyFlexJson).contains("\"label\":\".favorite\"");
	}

	@Test
	void testHandleInvalidTextMessageEvent() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage("random");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("Perintah tidak dikenali. " +
				"Balas dengan '.help' (tanpa tanda kutip) untuk melihat bantuan.",
				((TextMessage) reply).getText());
	}

	@Test
	void testHandleWikiFeatureHelpKeyword() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".wiki");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("Tampilkan halaman wikipedia tentang apa yang kamu cari.\n" +
						"Format: .wiki + [apa_yang_kamu_cari_dalam_bahasa_inggris]",
				((TextMessage) reply).getText());
	}

	@Test
	void testHandleFavoriteFeatureHelpKeyword() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".favorite");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("Untuk menggunakan fitur ini, kamu harus mendaftarkan akun LINE kamu ke " +
				"dalam database kami dengan keyword '.register' (tanpa tanda kutip). \n\n" +
				"Setelah berhasil terdaftar, gunakan keyword\n'.save [Nama Artikel] [URL Artikel]' " +
				"(tanpa tanda kutip) untuk menyimpan artikel yang kamu mau.\n\n" +
				"Untuk melihat artikel-artikel apa saja yang telah kamu simpan, gunakan keyword " +
				"'.showfavorites' (tanpa tanda kutip).",
				((TextMessage) reply).getText());
	}
	
	@Test
	void testHandleImageFeatureHelpKeyword() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".image");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("Fitur dimana kamu dapat memperoleh foto suatu hal yang kamu inginkan! \n\n" +
						"Gunakan keyword '.image [satu kata yang kamu cari dalam bahasa indonesia/inggris]'" +
						"(tanpa tanda kutip).\n\n" + "Contoh: .image rumah",
				((TextMessage) reply).getText());
	}

	@Test
	void testHandleImageFeatureKeyword() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".image minum");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("https://al-viraal.com/wp-content/uploads/2018/11/Video-Ini-Jawaban-Sandiaga-Uno-Sengaja-Minum-Kopi-Susu-Tanpa-Diaduk.jpg",
		((ImageMessage)reply).getOriginalContentUrl());
	}

	@Test
	void testHandleNewsFeatureHelpKeyword() throws IOException, ExecutionException, InterruptedException {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".news");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("Tampilkan berita terpopuler dan terbaru.\n" +
				"Gunakan keyword '.news semua' (tanpa tanda kutip).\n\n" +
				"Untuk menampilkan berita sesuai topik, gunakan keyword " +
				"'.news [topik]' (tanpa tanda kutip).\n" + "Contoh: .news pemilu",
				((TextMessage) reply).getText());
	}

    @Test
    void testHandleNewsFeatureSemua() throws IOException, ExecutionException, InterruptedException {
        MessageEvent<TextMessageContent> event = this.eventTestUtility
                .createDummyTextMessage(".news semua");
        Message reply = curiobotController.handleTextEvent(event);
        assertThat(((TextMessage) reply).getText().contains("Menunjukkan berita terbaru"));
    }

    @Test
    void testHandleNewsFeatureTopik() throws IOException, ExecutionException, InterruptedException {
        MessageEvent<TextMessageContent> event = this.eventTestUtility
                .createDummyTextMessage(".news topik prabowo");
        Message reply = curiobotController.handleTextEvent(event);
        assertThat(((TextMessage) reply).getText().contains("Menunjukkan berita terbarudengan topik prabowo"));
    }

	@Test
	void testHandleFactsFeatureHelpKeyword() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".facts");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("Tampilkan fakta secara acak.\n" +
				"Gunakan keyword '.facts random' (tanpa tanda kutip) untuk menampilkan" +
						" fakta secara acak.\n"+
						"Gunakan keyword '.facts today' (tanpa tanda kutip) untuk" +
						" menampilkan fakta harian yang terpilih.",
				((TextMessage) reply).getText());
	}

	@Test
	void testWikiFeatureSearchKeyword() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".wiki stack overflow");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("In software, a stack overflow occurs if the call stack pointer exceeds the stack bound. The call stack may consist of a limited amount of address space, often determined at the start of the program. The size of the call stack depends on many factors, including the programming language, machine architecture, multi-threading, and amount of available memory. When a program attempts to use more space than is available on the call stack (that is, when it attempts to access memory beyond the call stack's bounds, which is essentially a buffer overflow), the stack is said to overflow, typically resulting in a program crash....",
				((TextMessage) reply).getText());
	}

	@Test
	void testWikiIfNoArticleFound() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".wiki kucing");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("Artikel tidak ditemukan.",
				((TextMessage) reply).getText());
	}

	@Test
	void testHandleRegisterFeature() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".register");
		Message reply = curiobotController.handleTextEvent(event);
		assertEquals("Akun LINE Anda berhasil terdaftar. " +
						"Sekarang Anda dapat menggunakan fitur .favorite",
				((TextMessage) reply).getText());
	}

	@Test
	void testSaveUserFavoriteAfterRegistration() throws Exception {
		MessageEvent<TextMessageContent> eventSaveArticle = this.eventTestUtility
				.createDummyTextMessage(".save test artikel https://test.com");
		Message reply = curiobotController.handleTextEvent(eventSaveArticle);
		assertEquals("Artikel berhasil disimpan.",
				((TextMessage) reply).getText());
	}

	@Test
	void testGetUserFavoriteAfterSavingArticle() throws Exception {
		MessageEvent<TextMessageContent> eventSaveArticle = this.eventTestUtility
				.createDummyTextMessage(".save test_artikel https://test.com");
		curiobotController.handleTextEvent(eventSaveArticle);
		MessageEvent<TextMessageContent> eventShowArticles = this.eventTestUtility
				.createDummyTextMessage(".showfavorites");
		Message reply = curiobotController.handleTextEvent(eventShowArticles);
		assertThat(((TextMessage) reply).getText()).isNotBlank();
		assertThat(((TextMessage) reply).getText()).contains("Artikel Favorit Anda\n============\n");
		assertThat(((TextMessage) reply).getText()).contains("ID:");
		assertThat(((TextMessage) reply).getText()).contains("Judul:");
		assertThat(((TextMessage) reply).getText()).contains("Link:");
	}

	@Test
	void testGETRequestRandomFact() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".facts random");
		Message reply = curiobotController.handleTextEvent(event);
		assertThat(((TextMessage) reply).getText()).isNotBlank();
		assertThat(((TextMessage) reply).getText()).contains("Fact > ");
	}

	@Test
	void testGETRequestTodayFact() throws Exception {
		MessageEvent<TextMessageContent> event = this.eventTestUtility
				.createDummyTextMessage(".facts today");
		Message reply = curiobotController.handleTextEvent(event);
		assertThat(((TextMessage) reply).getText()).isNotBlank();
		assertThat(((TextMessage) reply).getText()).contains("Fact > ");
	}
}
