package group.c2.curiobot.favoriteserviceclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class LINEUser {

	@SerializedName("userId")
	@Expose
	private String userId;

	@SerializedName("favorites")
	@Expose
	private List<UserFavorite> favorites = new ArrayList<>();

	public LINEUser(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}
}
