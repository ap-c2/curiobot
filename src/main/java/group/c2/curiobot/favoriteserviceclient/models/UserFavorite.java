package group.c2.curiobot.favoriteserviceclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserFavorite {

	@SerializedName("id")
	@Expose
	private Long id;

	@SerializedName("title")
	@Expose
	private String title;

	@SerializedName("url")
	@Expose
	private String url;

	@SerializedName("owner")
	@Expose
	private LINEUser owner;

	public UserFavorite(String title, String url) {
		this.title = title;
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}
}
