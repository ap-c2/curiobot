package group.c2.curiobot.favoriteserviceclient;

import group.c2.curiobot.favoriteserviceclient.models.LINEUser;
import group.c2.curiobot.favoriteserviceclient.models.UserFavorite;
import org.springframework.web.bind.annotation.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.List;

public interface FavoriteArticleAPI {

	@GET("/users/{id}")
	Call<LINEUser> getLINEUserByUserId(@Path("id") String userId);

	@POST("/users")
	Call<LINEUser> saveLINEUser(@Body LINEUser user);

	@GET("/users/{userId}/favorites")
	Call<List<UserFavorite>> getFavoritesByUserId(@Path("userId") String userId);

	@POST("/users/{userId}/favorites")
	Call<UserFavorite> saveUserFavorite(@Path("userId") String userId,
	                                    @Body UserFavorite userFavorite);
}
