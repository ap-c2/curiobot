package group.c2.curiobot.favoriteserviceclient;

import group.c2.curiobot.favoriteserviceclient.models.LINEUser;
import group.c2.curiobot.favoriteserviceclient.models.UserFavorite;
import lombok.extern.slf4j.Slf4j;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class FavoriteArticleServiceController {

	private FavoriteArticleAPI httpClient = new Retrofit.Builder()
			.baseUrl("https://curiobot-favorite-article.herokuapp.com")
			.addConverterFactory(GsonConverterFactory.create())
			.build()
			.create(FavoriteArticleAPI.class);

	public CompletableFuture<LINEUser> getLINEUserByUserId(String userId) {
		Call<LINEUser> apiCall = httpClient.getLINEUserByUserId(userId);
		return CompletableFuture.supplyAsync(() -> {
			LINEUser fetchedLINEUser;
			try {
				fetchedLINEUser = apiCall.execute().body();
			} catch (IOException e) {
				e.printStackTrace();
				fetchedLINEUser = new LINEUser();
			}
			return fetchedLINEUser;
		});
	}

	public void saveLINEUser(LINEUser lineUser) {
		Call<LINEUser> apiCall = httpClient.saveLINEUser(lineUser);
		apiCall.enqueue(new Callback<LINEUser>() {
			@Override
			public void onResponse(Call<LINEUser> call, Response<LINEUser> response) {
				if (response.isSuccessful()) {
					log.info(response.body().getUserId());
				}
			}

			@Override
			public void onFailure(Call<LINEUser> call, Throwable t) {
				log.info(t.getMessage());
			}
		});
	}

	public CompletableFuture<List<UserFavorite>> getUserFavoriteById(String userId) {
		Call<List<UserFavorite>> apiCall = httpClient.getFavoritesByUserId(userId);
		return CompletableFuture.supplyAsync(() -> {
			List<UserFavorite> fetchedUserFavoriteList;
			try {
				fetchedUserFavoriteList = apiCall.execute().body();
			} catch (IOException e) {
				e.printStackTrace();
				fetchedUserFavoriteList = new ArrayList<>();
			}
			return fetchedUserFavoriteList;
		});
	}

	public void saveUserFavorite(String userId, UserFavorite userFavorite) {
		Call<UserFavorite> apiCall = httpClient.saveUserFavorite(userId, userFavorite);
		apiCall.enqueue(new Callback<UserFavorite>() {
			@Override
			public void onResponse(Call<UserFavorite> call, Response<UserFavorite> response) {
				if (response.isSuccessful()) {
					log.info(response.body().getTitle());
					log.info(response.body().getUrl());
				}
			}

			@Override
			public void onFailure(Call<UserFavorite> call, Throwable t) {
				log.info(t.getMessage());
			}
		});
	}
}
