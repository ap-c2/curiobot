package group.c2.curiobot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CuriobotApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuriobotApplication.class, args);
	}

}
