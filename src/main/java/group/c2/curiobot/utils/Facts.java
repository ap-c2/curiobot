package group.c2.curiobot.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Facts {

    public String GETRequest(URL url) throws IOException {
        String readLine = null;
        HttpURLConnection conection = (HttpURLConnection) url.openConnection();
        conection.setRequestMethod("GET");
        int responseCode = conection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conection.getInputStream()));
            StringBuffer response = new StringBuffer();
            while ((readLine = in.readLine()) != null) {
                response.append(readLine);
                response.append(System.lineSeparator());
            } in .close();
            String[] lines = response.toString().split("\\r?\\n") ;
            return ("Fact " + lines[0]);
        }
        else {
            return "Not Working";
        }
    }

    public String GETRequestRandom() throws IOException {
        URL urlForGetRequestRandomFact = new URL("http://randomuselessfact.appspot.com/random.txt?language=en");
        return GETRequest(urlForGetRequestRandomFact);
    }

    public String GETRequestToday() throws IOException {
        URL urlForGetRequestTodayFact = new URL("http://randomuselessfact.appspot.com/today.txt?language=en");
        return GETRequest(urlForGetRequestTodayFact);
    }


}
