package group.c2.curiobot.utils;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import group.c2.curiobot.FlexMessageSupplier;

public class ReplyHelpFlexMessage implements BotReply {

	@Override
	public Message reply(MessageEvent<TextMessageContent> messageEvent) {
		return new FlexMessageSupplier().get();
	}
}
