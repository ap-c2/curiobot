package group.c2.curiobot.utils;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.ImageMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReplyImageFeature implements BotReply {

	@Override
	public Message reply(MessageEvent<TextMessageContent> messageEvent) throws IOException {
		TextMessageContent userMessage = messageEvent.getMessage();
		String userMessageText = userMessage.getText();
		List<String> userMessageTextArray = new ArrayList<>(Arrays.asList(userMessageText.split(" ")));
		String replyMessage;
		if (userMessageTextArray.size() == 1) {
			replyMessage = helper.getImageFeatureHelpMessage();
			return new TextMessage(replyMessage);
		} else {
			String key = "AIzaSyCqXEkGFH7InRDEdmZj8a0x1zoX513oglk";

			StringBuilder stringTaker = new StringBuilder();
			for(int i=1; i<userMessageTextArray.size();i++){
				stringTaker.append(userMessageTextArray.get(i));
				stringTaker.append("%20");
			}
			String query = stringTaker.substring(0, stringTaker.length()-3);
			URL url = new URL("https://www.googleapis.com/customsearch/v1?key=" + key + "&cx=011858729392942046238:05ftiptlhag"
					+ "&q=" + query + "&searchType=image&imgSize=medium&num=1&start=1&alt=json");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			while ((output = br.readLine()) != null) {
				if (output.contains("\"link\": \"")) {
					String link = output.substring(output.indexOf("\"link\": \"") + ("\"link\": \"").length(), output.indexOf("\","));
					return new ImageMessage(link, link);
				}
			}
			return new TextMessage("Foto tidak ditemukan");

		}
	}
}
