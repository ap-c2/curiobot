package group.c2.curiobot.utils;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.util.concurrent.ExecutionException;

public class ReplyShowFavoriteArticleFeature implements BotReply {

	@Override
	public Message reply(MessageEvent<TextMessageContent> messageEvent) throws ExecutionException, InterruptedException {
		String userId = messageEvent.getSource().getUserId();
		String replyMessage = helper.getUserFavorites(userId);
		return new TextMessage(replyMessage);
	}
}
