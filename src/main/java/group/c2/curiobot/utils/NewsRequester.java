package group.c2.curiobot.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

public class NewsRequester {

    private static final String APIKEY = "4be97a347ffc42ddb541dd9b388d32a4";
    private static String source = "https://newsapi.org/v2/";

    private static String getSource(String topik) {
        String endpoint = "";
        if (topik != null){
            endpoint += "everything?q=" + topik;
        }else {
            endpoint += "top-headlines?country=id";
        }
        return NewsRequester.source + endpoint + "&apiKey=" + APIKEY;
    }

    public ArrayList<Map<String, Object>> getNews(String topik) throws IOException {
        URL url = new URL(getSource(topik));
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) {
            ObjectMapper mapper = new ObjectMapper();
            InputStreamReader inStream = new InputStreamReader(
                    connection.getInputStream());
            Map<String, Object> newsMap = mapper.readValue(
                    inStream, new TypeReference<Map<String, Object>>(){});

            if (newsMap.get("articles") != null) {
                @SuppressWarnings("unchecked")
                ArrayList<Map<String, Object>> newsArray =
                        (ArrayList<Map<String, Object>>) newsMap.get("articles");
                return newsArray ;
            } else {
                return null;
            }
        }else {
            throw new ConnectException();
        }
    }
}
