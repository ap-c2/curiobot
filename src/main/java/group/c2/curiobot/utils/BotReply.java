package group.c2.curiobot.utils;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public interface BotReply {

	BotHelper helper = new BotHelper();

	Message reply(MessageEvent<TextMessageContent> messageEvent) throws ExecutionException, InterruptedException, IOException;
}
