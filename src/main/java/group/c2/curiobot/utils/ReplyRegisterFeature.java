package group.c2.curiobot.utils;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class ReplyRegisterFeature implements BotReply {

	@Override
	public Message reply(MessageEvent<TextMessageContent> messageEvent) {
		String userId = messageEvent.getSource().getUserId();
		String replyMessage = helper.registerUser(userId);
		return new TextMessage(replyMessage);
	}
}
