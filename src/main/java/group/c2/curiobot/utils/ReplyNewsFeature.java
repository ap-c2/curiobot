package group.c2.curiobot.utils;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.io.IOException;
import java.net.ConnectException;
import java.util.*;

public class ReplyNewsFeature implements BotReply {

	@Override
	public Message reply(MessageEvent<TextMessageContent> messageEvent) throws IOException {
		TextMessageContent userMessage = messageEvent.getMessage();
		String userMessageText = userMessage.getText();
		List<String> userMessageTextArray = new ArrayList<>(
				Arrays.asList(userMessageText.split(" ")));

		String replyMessage;
		if (userMessageTextArray.size() == 2) {
			try {
				String topik = userMessageTextArray.get(1).toLowerCase();
				if (topik.equals("semua")) {
					topik = null;
				}
				ArrayList<Map<String, Object>> content = new NewsRequester().getNews(topik);
				if (content != null) {
					replyMessage = getreply(content, topik);
				} else {
					replyMessage = "Tidak dapat memperoleh berita terbaru dengan topik " + topik;
				}
			} catch (ConnectException e) {
				replyMessage = "Tidak dapat terhubung dengan layanan " +
						"untuk mendapatkan berita terbaru";
			}
		}  else {
			replyMessage = helper.getNewsFeatureHelpMessage();
		}
		return new TextMessage(replyMessage);
	}

	private String getreply(ArrayList<Map<String, Object>> newsArray, String topik) {
		StringBuilder reply = new StringBuilder();
		reply.append("Menunjukkan berita terbaru");
		String addition = "\n\n";
		if (topik != null) {
			addition = "dengan topik " + topik + "\n\n";
		}
		reply.append(addition);

		Iterator<Map<String, Object>> arrayIterator = newsArray.iterator();
		int i = 0;
		while (i<5 && arrayIterator.hasNext()) {
			Map<String, Object> content = arrayIterator.next();
			reply.append((content.get("title") + "\n" +
					content.get("url") + "\n\n"));
			i++;
		}
		reply.append("Powered by News API\n" + "https://newsapi.org");
		return reply.toString();
	}
}
