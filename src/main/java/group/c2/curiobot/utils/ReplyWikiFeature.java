package group.c2.curiobot.utils;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.util.*;

public class ReplyWikiFeature implements BotReply {

	@Override
	public Message reply(MessageEvent<TextMessageContent> messageEvent) {
		TextMessageContent userMessage = messageEvent.getMessage();
		String userMessageText = userMessage.getText();
		List<String> userMessageTextArray = new ArrayList<>(Arrays.asList(userMessageText.split(" ")));
		String replyMessage;
		if (userMessageTextArray.size() == 1) {
			replyMessage = helper.getWikiFeatureHelpMessage();
		} else {
			userMessageTextArray.remove(0);
			String wordToSearch = String.join(" ", userMessageTextArray);
			replyMessage = helper.getWikiText(wordToSearch);
			if (checkIfReplyIsEmpty(replyMessage)){
			    replyMessage = "Artikel tidak ditemukan.";
            }
		}
		return new TextMessage(replyMessage);
	}

	public boolean checkIfReplyIsEmpty(String replyMessage) {
		if(replyMessage.equals("...")){
			return true;
		}
		else {
		    return false;
        }
	}
}
