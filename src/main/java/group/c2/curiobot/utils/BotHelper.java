package group.c2.curiobot.utils;

import fastily.jwiki.core.*;
import group.c2.curiobot.favoriteserviceclient.FavoriteArticleServiceController;
import group.c2.curiobot.favoriteserviceclient.models.LINEUser;
import group.c2.curiobot.favoriteserviceclient.models.UserFavorite;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class BotHelper {

	private FavoriteArticleServiceController favoriteArticleServiceController = new FavoriteArticleServiceController();

	public String getFavoriteFeatureHelpMessage() {
		return "Untuk menggunakan fitur ini, kamu harus mendaftarkan akun LINE kamu ke " +
				"dalam database kami dengan keyword '.register' (tanpa tanda kutip). \n\n" +
				"Setelah berhasil terdaftar, gunakan keyword\n'.save [Nama Artikel] [URL Artikel]' " +
				"(tanpa tanda kutip) untuk menyimpan artikel yang kamu mau.\n\n" +
				"Untuk melihat artikel-artikel apa saja yang telah kamu simpan, gunakan keyword " +
				"'.showfavorites' (tanpa tanda kutip).";
	}

	public String getWikiFeatureHelpMessage() {
		return "Tampilkan halaman wikipedia tentang apa yang kamu cari.\n" +
				"Format: .wiki + [apa_yang_kamu_cari_dalam_bahasa_inggris]";
	}

	public String getImageFeatureHelpMessage(){
		return "Fitur dimana kamu dapat memperoleh foto suatu hal yang kamu inginkan! \n\n" +
				"Gunakan keyword '.image [satu kata yang kamu cari dalam bahasa indonesia/inggris]'" + "(tanpa tanda kutip).\n\n" + "Contoh: .image rumah";
	}

	public String getNewsFeatureHelpMessage(){
		return "Tampilkan berita terpopuler dan terbaru.\n" +
				"Gunakan keyword '.news semua' (tanpa tanda kutip).\n\n" +
				"Untuk menampilkan berita sesuai topik, gunakan keyword " +
				"'.news [topik]' (tanpa tanda kutip).\n" + "Contoh: .news pemilu";
	}

	public String getFactsFeatureHelpMessage(){
		return "Tampilkan fakta secara acak.\n" +
				"Gunakan keyword '.facts random' (tanpa tanda kutip) untuk menampilkan fakta secara acak.\n" +
				"Gunakan keyword '.facts today' (tanpa tanda kutip) untuk menampilkan fakta harian yang terpilih.";
	}

	public String getWikiText(String wordToSearch) {
		Wiki wiki = new Wiki("en.wikipedia.org");
		String firstParagraph = wiki.getTextExtract(wordToSearch);
		String replyString = firstParagraph.substring(0, Math.min(firstParagraph.length(), 1995));
		replyString += "...";
		return replyString;
	}

	public String registerUser(String userId) {
		favoriteArticleServiceController.saveLINEUser(new LINEUser(userId));
		return "Akun LINE Anda berhasil terdaftar. Sekarang Anda dapat menggunakan fitur .favorite";
	}

	public String saveUserFavorite(String userId, String title, String url) throws ExecutionException, InterruptedException {
		String replyMessage;
		CompletableFuture<LINEUser> apiResponse = favoriteArticleServiceController.getLINEUserByUserId(userId);
		if (apiResponse.get() == null) {
			replyMessage = "Akun Anda belum terdaftar. Gunakan keyword .register untuk mendaftarkan akun Anda";
		} else {
			if (!url.contains("http")) {
				replyMessage = "URL yang disimpan harus memuat awalan http atau https.";
			} else {
				favoriteArticleServiceController.saveUserFavorite(userId, new UserFavorite(title, url));
				replyMessage = "Artikel berhasil disimpan.";
			}

		}
		return replyMessage;
	}

	public String getUserFavorites(String userId) throws ExecutionException, InterruptedException {
		StringBuffer replyMessage = new StringBuffer();
		CompletableFuture<LINEUser> userApiResponse = favoriteArticleServiceController.getLINEUserByUserId(userId);
		if (userApiResponse.get() == null) {
			replyMessage.append("Akun Anda belum terdaftar. Gunakan keyword .register untuk mendaftarkan akun Anda");
		} else {
			CompletableFuture<List<UserFavorite>> favoriteArticleApiResponse = favoriteArticleServiceController
					.getUserFavoriteById(userId);
			List<UserFavorite> retrievedUserFavorites = favoriteArticleApiResponse.get();
			if (!retrievedUserFavorites.isEmpty()) {
				replyMessage.append("Artikel Favorit Anda\n============\n");
				for (UserFavorite article : retrievedUserFavorites) {
					replyMessage.append("ID: ").append(article.getId())
							.append("\nJudul: ").append(article.getTitle())
							.append("\nLink: ").append(article.getUrl()).append("\n\n");
				}
			} else {
				replyMessage.append("Anda belum menyimpan artikel apapun. " +
						"Gunakan '.save [judul artikel] [url]' (tanpa tanda kutip)");
			}
		}
		return replyMessage.toString();
	}
}
