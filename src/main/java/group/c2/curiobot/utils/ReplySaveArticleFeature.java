package group.c2.curiobot.utils;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ReplySaveArticleFeature implements BotReply {

	@Override
	public Message reply(MessageEvent<TextMessageContent> messageEvent) throws ExecutionException, InterruptedException {
		TextMessageContent userMessage = messageEvent.getMessage();
		String userMessageText = userMessage.getText();
		List<String> userMessageTextArray = new ArrayList<>(Arrays.asList(userMessageText.split(" ")));
		String replyMessage;
		if (userMessageTextArray.size() >= 3) {
			String userId = messageEvent.getSource().getUserId();
			String url = userMessageTextArray.get(userMessageTextArray.size() - 1);
			userMessageTextArray.remove(0);
			userMessageTextArray.remove(userMessageTextArray.size() - 1);
			String title = String.join(" ", userMessageTextArray);
			replyMessage = helper.saveUserFavorite(userId, title, url);
		} else {
			replyMessage = "Format perintah salah. Gunakan .save [judul] [URL]";
		}
		return new TextMessage(replyMessage);
	}
}
