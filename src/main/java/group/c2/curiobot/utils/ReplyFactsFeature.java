package group.c2.curiobot.utils;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ReplyFactsFeature implements BotReply {

	@Override
	public Message reply(MessageEvent<TextMessageContent> messageEvent) throws ExecutionException, InterruptedException, IOException {
		TextMessageContent userMessage = messageEvent.getMessage();
		String userMessageText = userMessage.getText();
		List<String> userMessageTextArray = new ArrayList<>(Arrays.asList(userMessageText.split(" ")));
		String replyMessage;
		if (userMessageTextArray.size() == 1) {
			replyMessage = helper.getFactsFeatureHelpMessage();
		} else {
			if (userMessageTextArray.get(1).equals("random")) {
				replyMessage = new Facts().GETRequestRandom();
			}
			else if (userMessageTextArray.get(1).equals("today")) {
				replyMessage = new Facts().GETRequestToday();
			} else {
				replyMessage = helper.getFactsFeatureHelpMessage();
			}
		}
		return new TextMessage(replyMessage);
	}
}
