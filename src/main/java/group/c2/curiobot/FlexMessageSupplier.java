package group.c2.curiobot;

import static java.util.Arrays.asList;


import java.util.Collections;
import java.util.function.Supplier;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectMode;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectRatio;
import com.linecorp.bot.model.message.flex.component.Image.ImageSize;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

public class FlexMessageSupplier implements Supplier<FlexMessage> {
    @Override
    public FlexMessage get() {

        final Image heroBlock =
                Image.builder()
                        .url("https://i.ibb.co/BrNz4DL/robot-1.png")
                        .size(ImageSize.LG)
                        .aspectRatio(ImageAspectRatio.R1_51TO1)
                        .aspectMode(ImageAspectMode.Fit)
                        .build();

        final Box bodyBlock = createBodyBlock();
        final Box footerBlock = createFooterBlock();
        final Bubble bubble =
                Bubble.builder()
                        .hero(heroBlock)
                        .body(bodyBlock)
                        .footer(footerBlock)
                        .direction(FlexDirection.LTR)
                        .build();

        return new FlexMessage("HELP", bubble);
    }

    private Box createBodyBlock() {
        final Text title =
                Text.builder()
                        .text("FEATURES")
                        .size(FlexFontSize.XL)
                        .align(FlexAlign.CENTER)
                        .color("#000000")
                        .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(Collections.singletonList(title))
                .build();
    }

    private Box createFooterBlock() {
        final Button wikiAction = Button
                .builder()
                .style(ButtonStyle.PRIMARY)
                .height(ButtonHeight.SMALL)
                .margin(FlexMarginSize.XS)
                .color("#000000")
                .action(new MessageAction(".wiki", ".wiki"))
                .build();

        final Button newsAction =
                Button.builder()
                        .style(ButtonStyle.SECONDARY)
                        .height(ButtonHeight.SMALL)
                        .margin(FlexMarginSize.XS)
                        .color("#E2E2E2")
                        .action(new MessageAction(".news", ".news"))
                        .build();

        final Button imageAction = Button
                .builder()
                .style(ButtonStyle.PRIMARY)
                .height(ButtonHeight.SMALL)
                .margin(FlexMarginSize.XS)
                .color("#000000")
                .action(new MessageAction(".image", ".image"))
                .build();

        final Button factsAction =
                Button.builder()
                        .style(ButtonStyle.SECONDARY)
                        .height(ButtonHeight.SMALL)
                        .margin(FlexMarginSize.XS)
                        .color("#E2E2E2")
                        .action(new MessageAction(".facts", ".facts"))
                        .build();

        final Button favoriteAction = Button
                .builder()
                .style(ButtonStyle.PRIMARY)
                .height(ButtonHeight.SMALL)
                .margin(FlexMarginSize.XS)
                .color("#000000")
                .action(new MessageAction(".favorite", ".favorite"))
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(asList(wikiAction, newsAction, imageAction, factsAction, favoriteAction))
                .build();
    }
}
