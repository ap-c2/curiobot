package group.c2.curiobot;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import group.c2.curiobot.utils.*;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@SuppressWarnings("ALL")
@LineMessageHandler
public class CuriobotController extends SpringBootServletInitializer {

	private static Map<String, BotReply> botReplyHandlers = new HashMap<>();

	static {
		botReplyHandlers.put(".favorite", new ReplyFavoriteFeatureHelp());
		botReplyHandlers.put(".help", new ReplyHelpFlexMessage());
		botReplyHandlers.put(".wiki", new ReplyWikiFeature());
		botReplyHandlers.put(".image", new ReplyImageFeature());
		botReplyHandlers.put(".news", new ReplyNewsFeature());
		botReplyHandlers.put(".register", new ReplyRegisterFeature());
		botReplyHandlers.put(".save", new ReplySaveArticleFeature());
		botReplyHandlers.put(".showfavorites", new ReplyShowFavoriteArticleFeature());
		botReplyHandlers.put(".facts", new ReplyFactsFeature());
	}

	public CuriobotController(LineMessagingClient lineMessagingClient) {
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(CuriobotApplication.class);
	}

	@EventMapping
	public Message handleTextEvent(MessageEvent<TextMessageContent> event) throws ExecutionException,
			InterruptedException, IOException {
		return handleMessageKeyword(event);
	}

	private Message handleMessageKeyword(MessageEvent<TextMessageContent> event) throws IOException,
			ExecutionException, InterruptedException {
		TextMessageContent messageContent = event.getMessage();
		String userMessageText = messageContent.getText();
		String[] userMessageSplit = userMessageText.split(" ");

		try {
			BotReply replyHandler = botReplyHandlers.get(userMessageSplit[0]);
			return replyHandler.reply(event);
		} catch (NullPointerException e) {
			return new TextMessage("Perintah tidak dikenali. " +
					"Balas dengan '.help' (tanpa tanda kutip) untuk melihat bantuan.");
		}
	}
}
