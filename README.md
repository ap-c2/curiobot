# Curio Bot

[![pipeline status](https://gitlab.com/ap-c3/curiobot/badges/master/pipeline.svg)](https://gitlab.com/ap-c3/curiobot/commits/master)
[![coverage report](https://gitlab.com/ap-c3/curiobot/badges/master/coverage.svg)](https://gitlab.com/ap-c3/curiobot/commits/master)

**Proyek Akhir Kelompok C2 Mata Kuliah Advanced Programming, Fakultas Ilmu Komputer, Universitas Indonesia, Semester Genap 2018/2019**

Contributors:

1. [**Alvin Raihan**](https://gitlab.com/alvinraih) - **1506689074**
2. [**Darin Amanda Zakiya**](https://gitlab.com/darinamanda) - **1706979190**
3. [**Devin Winardi**](https://gitlab.com/devin1727) - **1706039755**
4. [**Farhan Azyumardhi Azmi**](https://gitlab.com/ZoneZ) - **1706979234**
5. [**M. Abdurrahman**](https://gitlab.com/apateon) - **1706039925**

## Penjelasan Proyek

CurioBot (didapat dari kata Curiosity) merupakan sebuah chatbot yang dikembangkan di platform komunikasi Line untuk membantu menjawab ketidaktahuan atau rasa penasaran user terhadap definisi suatu hal. Seluruh informasi yang dikirimkan kepada user akan didapatkan dari API. Keyword yang dimasukkan user akan dikirimkan ke API dan direturn dalam bentuk JSON. JSON tersebut kemudian akan diolah agar dapat ditampilkan kepada user. Setelah user mengirim keyword yang diinginkan, dapat ditampilkan
informasi mengenai kata yang dicari berupa penjelasan dari kata tersebut atau foto dari kata tersebut. CurioBot juga dapat memberikan fakta harian maupun berita-berita kepada user jika user menulis keyword untuk fakta atau berita. User juga dapat menambah dan menghapus artikel favoritnya.

**ID Line : @upb0588c**

## Manfaat Proyek

Menjadi sumber informasi yang praktis, mudah, dan menyenangkan untuk digunakan. Selain itu, aplikasi ini dapat menjadi alternatif untuk mencari informasi dengan cepat.


## Fitur

1. Menerima kata dari user dan menampilkan hasil berupa deskripsi singkat, URL artikel tersebut di Wikipedia, dan foto dari kata tersebut. (Devin)
2. Menerima kata dari user dan menampilkan hasil berupa foto-foto yang berkaitan dengan kata tersebut dari Google Images. (Darin)
3. User dapat mencari berita-berita berdasarkan kata kunci dari user, melalui API penyedia berita seperti News API. Representasi tampilan mirip seperti fitur. (ABD)
4. User dapat meminta bot untuk mengeluarkan fact of the day secara acak via sebuah API. (Alvin)
5. User dapat melakukan bookmark atau menyimpan artikel favorit dari hasil pencariannya. (Farhan)

## Teknologi Digunakan

- [**Line**](https://line.me/en/): sebuah aplikasi sosial media dimana chat bot ini dapat digunakan.
- [**Java Spring Boot**](https://spring.io/projects/spring-boot): 
sebuah framework yang digunakan untuk mengembangkan aplikasi CurioBot. Berbasis bahasa pemrograman **Java**.
- **Wikipedia API, Google Images API(third-party), News API, Facts of The Day API**
- **Retrofit2**

## Heroku App URL

https://curio-bot.herokuapp.com/

## Developer Section

- Membuat branch baru: `git checkout -b [nama-branch]`
- Pindah branch: `git checkout [nama-branch]`

### Additional Resources

- [**Line Messaging API Reference**](https://developers.line.biz/en/reference/messaging-api/)

